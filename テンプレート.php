<!doctype html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>売上登録確認</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">
</head>

<body>
        <div class="container">
                <div class="row">
                  <div class="col-sm-2">お名前</div>
                  <div class="col-sm-10 form-inline" style="padding: 3px;">
                    <input type="text" class="form-control input-sm" id="name" placeholder="お名前" size="20">
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-2">フリガナ</div>
                  <div class="col-sm-10 form-inline" style="padding: 3px;">
                    <input type="text" class="form-control input-sm" id="kana" placeholder="フリガナ" size="20">
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-2">都道府県</div>
                  <div class="col-sm-10 form-inline" style="padding: 3px;">
                    <select class="form-control input-sm" id="pref">
                      <option>北海道</option>
                      <option>青森</option>
                      <option>秋田</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-2">ご住所</div>
                  <div class="col-sm-10" style="padding: 3px;">
                    <input type="text" class="form-control input-sm" id="address" placeholder="ご住所">
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-2">Email</div>
                  <div class="col-sm-10 form-inline" style="padding: 3px;">
                    <input type="text" class="form-control input-sm" id="email" placeholder="Email" size="30">
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-2">ご意見をどうぞ</div>
                  <div class="col-sm-10" style="padding: 3px;">
                    <textarea class="form-control  input-sm" rows="3" id="comment" placeholder="ご意見をどうぞ"></textarea>
                  </div>
                </div>
                 
                <div class="text-center" style="padding: 30px;">
                  <button type="button" class="btn btn-success">送信内容の確認 <span class="glyphicon glyphicon-chevron-right"></span></button>
                </div>
              </div> 
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ"
        crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js"></script>
</body>

</html>