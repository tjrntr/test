<?php
session_start();
include("./app/sale.php");

$res = array();
if (isset($_SESSION["search_info"])) {
    $db = new Sale();
    $sale_date = $_SESSION["search_info"]["day1"];
    $sale_date2 = $_SESSION["search_info"]["day2"];
    $charge_id = $_SESSION["search_info"]["charge"];
    $category_id = $_SESSION["search_info"]["category"];
    $product_name = $_SESSION["search_info"]["product"];
    $sale_remarks = $_SESSION["search_info"]["remarks"];

    $res = $db->select($sale_date, $sale_date2, $charge_id, $category_id, $product_name, $sale_remarks);
}

// var_dump($res);
?>



<!doctype html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>売上検索結果表示</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">物品売上管理システム</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav mr-auto">
                    <li><a href="./Dashboad.php">ダッシュボード</a></li>
                    <li><a href="./s0010.php">売上登録</a></li>
                    <li class="active"><a href="#">売上検索<span class="sr-only">(current)</span></a></li>
                    <li><a href="./アカウント登録.html">アカウント登録</a></li>
                    <li><a href="./アカウント検索条件入力.html">アカウント検索</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">ログアウト</a></li>
                </ul>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->

    </nav>
    <div class="container">
        <form class="form-horizontal">
            <p>
                <h1>売上検索条件結果表示</h1>
                <br>
            </p>
            <table class="table">
                <thead>
                    <tr>
                        <th>操作</th>
                        <th>NO.</th>
                        <th>販売日</th>
                        <th>担当</th>
                        <th>商品カテゴリ</th>
                        <th>商品名</th>
                        <th>単価</th>
                        <th>個数</th>
                        <th>小計</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <?php
                        foreach ($res as $row) {
                                    $id = $row["sale_id"];
                                    echo "<tr>";
                                    echo '<th><a href="./s0022.php?id='.$id.'"><button type="button" name = "btn" class="btn btn-primary btn-sm">✔詳細</button></a></th>';
                                    echo "<td>" . $row["sale_id"] . "</td>";
                                    echo "<td>" . $row["sale_date"] . "</td>";
                                    echo "<td>" . $row["charge_name"] . "</td>";
                                    echo "<td>" . $row["category_name"] . "</td>";
                                    echo "<td>" . $row["product_name"] . "</td>";
                                    echo "<td>" . $row["product_price"] . "</td>";
                                    echo "<td>" . $row["product_count"] . "</td>";
                                    echo "<td>" . $row["product_price"] *$row["product_count"] . "</td>";
                                    echo "</tr>";
                                }
                        ?>

                            </tr>


                        </tbody>
                    </table>
                </form>
            </div>


            <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
            <script src="./js/bootstrap.min.js"></script>
        </body>

        </html>