
<?php
include("./app/sale2.php");

$res = array();
if (isset($_GET["id"])) {
    $db = new Sale2();
    $sale_id = $_GET["id"];
    $res = $db->select($sale_id);
}
// var_dump($res);

foreach ($res as $row){
    $id=$row["sale_id"];
    $date = $row["sale_date"];
    $charge = $row["charge_name"];
    $category = $row["category_name"];
    $name = $row["product_name"];
    $price = $row["product_price"];
    $count = $row["product_count"];
    $total = $row["product_price"] *$row["product_count"];
    $remarks = $row["sale_remarks"];
}

?>


<!doctype html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>売上詳細削除確認</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">物品売上管理システム</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav mr-auto">
                        <li class="active"><a href="#">ダッシュボード<span class="sr-only">(current)</span></a></li>
                        <li><a href="./売上登録.html">売上登録</a></li>
                        <li><a href="./売上検索条件入力.html">売上検索</a></li>
                        <li><a href="./アカウント登録.html">アカウント登録</a></li>
                        <li><a href="./アカウント検索条件入力.html">アカウント検索</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">ログアウト</a></li>
                </ul>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->

    </nav>
    <div class="container">
        <form class="form-horizontal" action="./app/sale_delete.php">
            <input type="hidden" name="id" value=<?php echo $id; ?>>
                <p>
                    <h1>売上詳細削除確認</h1>
                    <br>
                </p>
    
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">販売日</label>
                <div class="col-sm-4">
                    <input readonly type="text" class="form-control" value="<?php echo $date; ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">担当</label>
                <div class="col-sm-5">
                    <select readonly class="form-control">
                        <option><?php echo $charge; ?></option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">商品カテゴリ</label>
                <div class="col-sm-5">
                    <select readonly class="form-control">
                        <option><?php echo $category; ?></option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">商品名</label>
                <div class="col-sm-5 col-offset-sm-2">
                    <input readonly type="text" class="form-control"  value="<?php echo $name; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">単価</label>
                <div class="col-sm-4 col-offset-sm-6">
                    <input readonly type="text" class="form-control"  value="<?php echo $price; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">個数</label>
                <div class="col-sm-4 col-offset-sm-6">
                    <input readonly type="text" class="form-control"  value="<?php echo $count; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">小計</label>
                <div class="col-sm-4 col-offset-sm-6">
                    <input readonly type="text" class="form-control" value="<?php echo $total; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">備考</label>
                <div class="col-sm-4 col-offset-sm-6">
                    <textarea readonly class="form-control" rows="4" ><?php echo $remarks; ?></textarea>
                </div>
            </div>



            <div class="form-group">
                <div class="col-sm-offset-4">
                    <button type="submit" class="btn btn-danger btn-lg">✖OK</button>
                    <button type="button" class="btn btn-default btn-lg">キャンセル</button>
                </div>
            </div>
        </form>

    </div>
    <!--container-fruid-->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ"
        crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js"></script>
</body>

</html>