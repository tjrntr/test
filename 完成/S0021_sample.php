<?php
include("./app/sale.php");
$res=array();
if(isset($_POST["search"])){
    $db = new Sale();
    $day1 = $_POST["sales_day_from"];
    $day2 = $_POST["sales_day_to"];
    $charge = $_POST["charge"];
    $category = $_POST["product_category"];
    $product = $_POST["product_name"];
    $remarks = $_POST["remarks"];
    
    $res = $db->select($day1,$day2,$charge,$category,$product,$remarks);    
}
?>

<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>売上検索結果表示|物品売上管理システム</title>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/style.css">
</head>

<body>
    <!-- navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">物品売上管理システム</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="C0020.php">ダッシュボード</a></li>
                    <li><a href="S0010.php">売上登録</a></li>
                    <li><a href="S0020.php">売上検索</a></li>
                    <li><a href="S0030.php">アカウント登録</a></li>
                    <li><a href="S0040.php">アカウント検索</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="C0010.php">ログアウト</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- navbar -->

    <div class="container">

        <h1 class="h1">売上検索結果表示</h1>
        <div class="container">

            <table class="table">
                <tr>
                    <th>操作</th>
                    <th>No</th>
                    <th>販売日</th>
                    <th>担当</th>
                    <th>商品カテゴリー</th>
                    <th>商品名</th>
                    <th>単価</th>
                    <th>個数</th>
                    <th>小計</th>
                </tr>
                <?php
                foreach($res as $row){
                    echo "<tr>";
                ?>
                    <td>
                    <button type="submit" class="btn btn-primary">
                        <span class="glyphicon glyphicon-ok"></span> 詳細
                    </button>
                    </td>
                <?php
                    echo "<td>".$row["sale_id"]."</td>";
                    echo "<td>".$row["sale_date"]."</td>";
                    echo "<td>".$row["charge_name"]."</td>";
                    echo "<td>".$row["category_name"]."</td>";
                    echo "<td>".$row["product_name"]."</td>";
                    echo "<td>".$row["product_price"]."</td>";
                    echo "<td>".$row["product_count"]."</td>";
                    echo "<td>".($row["product_price"] * $row["product_count"])."</td>";
                    echo "</tr>";
                }
                ?>
            </table>
        </div>
    </div>

    <script src="./js/jquery-1.12.4.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
</body>

</html>