<?php
session_start();

if(isset($_GET["btn"])){
    unset($_SESSION["search_info"]);
    $_SESSION["search_info"]["day1"] = $_GET["sale_date"];
    $_SESSION["search_info"]["day2"] = $_GET["sale_date2"];
    $_SESSION["search_info"]["charge"] = $_GET["charge_id"];
    $_SESSION["search_info"]["category"] = $_GET["category_id"];
    $_SESSION["search_info"]["product"] = $_GET["product_name"];
    $_SESSION["search_info"]["remarks"] = $_GET["sale_remarks"];
    
    
        header("location:./s0021.php");
        // var_dump($_SESSION["search_info"]["day1"]);
}
include("./app/category.php");
include("./app/charge.php");

$db_cate = new Category;
$cateList = $db_cate->select();

$db_charge = new Charge;
$chargeList = $db_charge->select();

?>


<!doctype html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>売上検索条件入力</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">
</head>

<body>
        <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">物品売上管理システム</a>
                    </div>
        
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav mr-auto">
                            <li><a href="./Dashboad.php">ダッシュボード</a></li>
                            <li><a href="./s0010.php">売上登録</a></li>
                            <li class="active"><a href="./s0020.php">売上検索<span class="sr-only">(current)</span></a></li>
                            <li><a href="./アカウント登録.html">アカウント登録</a></li>
                            <li><a href="./アカウント検索条件入力.html">アカウント検索</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="./logout.php">ログアウト</a></li> 
                        </ul>
                              
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
                
            </nav>
    <div class="container">
        <form class="form-horizontal" action="s0020.php" method="GET">
            <p>
                <h1>売上検索条件入力</h1>
                <br>
            </p>
            <div class="form-group inline-block ">
                <label for="inputEmail3" class="col-sm-2 control-label">販売日</label>
                <div class="col-sm-3">
                    <input type="date" name ="sale_date" class="form-control" >
                </div>
                <div class="col-sm-1 text-center">
                    ～
                </div>
                <div class="col-sm-3">
                    <input type="date" name = "sale_date2" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">担当</label>
                <div class="col-sm-5">
                    <select class="form-control" name ="charge_id">
                    <option value="">選択してください</option>
                    <?php
							foreach($chargeList as $row){
								echo "<option value={$row['charge_id']}>";
								echo $row['charge_name'];
								echo "</option>";
							}
							?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">商品カテゴリ</label>
                <div class="col-sm-5">
                    <select class="form-control" name ="category_id">
                    <option value="">選択してください</option>
                    <?php
							foreach($cateList as $row){
								echo "<option value={$row['category_id']}>";
								echo $row['category_name'];
								echo "</option>";
							}
							?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">商品名<span
                        class="label label-default">部分一致</span></label>
                <div class="col-sm-6 col-offset-sm-4">
                    <input type="text" name="product_name" class="form-control"  placeholder="商品名">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">備考<span
                        class="label label-default">部分一致</span></label>
                <div class="col-sm-6 col-offset-sm-4">
                    <input type="text" name="sale_remarks" class="form-control" placeholder="備考">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-4">
                    
                    <button type="submit" name="btn" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-search"></span>検索</button>
                    
                    <button type="reset" class="btn btn-default btn-lg">クリア</button>
                </div>
            </div>
        </form>

    </div>
    <!--container-fruid-->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ"
        crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js"></script>
</body>

</html>

<?php


?>