<?php
session_start();
include("./app/charge.php");

$res = array();
if (isset($_SESSION["search_info"])) {
    $db = new Charge();
    $name = $_SESSION["search_info"]["name"];
    $mail = $_SESSION["search_info"]["mail"];
    $auth = $_SESSION["search_info"]["auth"];

    $res = $db->search($name, $mail, $auth);
}

// var_dump($res);
?>

<!doctype html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>アカウント検索結果表示</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">物品売上管理システム</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav mr-auto">
                    <li class="active"><a href="./Dashboad.html">ダッシュボード<span class="sr-only">(current)</span></a></li>
                    <li><a href="./売上登録.html">売上登録</a></li>
                    <li><a href="./売上検索条件入力.html">売上検索</a></li>
                    <li><a href="./アカウント登録.html">アカウント登録</a></li>
                    <li><a href="./アカウント検索条件入力.html">アカウント検索</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">ログアウト</a></li>
                </ul>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->

    </nav>
    <div class="container-fruid">
        <form class="form-horizontal">
            <p>
                <h1>アカウント検索条件結果表示</h1>
                <br>
            </p>
            <table class="table">
                <thead>
                    <tr>
                        <th>操作</th>
                        <th>NO.</th>
                        <th>氏名</th>
                        <th>メールアドレス</th>
                        <th>権限</th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                    foreach ($res as $row) {
                        if ($row["charge_auth_level"] == 1) {
                            $a = "権限なし";
                        } elseif ($row["charge_auth_level"] == 2) {
                            $a = "売上登録";
                        } elseif ($row["charge_auth_level"] == 3) {
                            $a = "アカウント登録";
                        }
                        $id=$row["charge_id"];
                        echo "<tr>";
                        echo "<th>";
                        echo "<a href='./s0042.php?id=$id'><button type='button' class='btn btn-primary btn-sm'>✔編集</button></a>";
                        echo "　";
                        echo "<a href='./s0044.php?id=$id'><button type='button' class='btn btn-danger btn-sm'>✖削除</button></a>";
                        echo "</th>";
                        echo "<td>" . $row["charge_id"] . "</td>";
                        echo "<td>" . $row["charge_name"] . "</td>";
                        echo "<td>" . $row["charge_mail"] . "</td>";
                        echo "<td>" . $a . "</td>";
                        echo "</tr>";
                    }
                    ?>

                </tbody>
            </table>
        </form>
    </div>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js"></script>
</body>

</html>