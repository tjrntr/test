<?php
session_start();
include("./app/category.php");
include("./app/charge.php");

$db_cate = new Category;
$cateList = $db_cate->select();

$db_charge = new Charge;
$chargeList = $db_charge->select();

foreach ($chargeList as $row) {
    if ($_POST["charge"] == $row["charge_name"]) {
        $charge = $row["charge_id"];      
    }
}

foreach ($cateList as $row2) {
    if ($_POST["category"] == $row2["category_name"]) {
        $category = $row2["category_id"];  
    }
}
?>



<!doctype html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>売上詳細編集確認</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">物品売上管理システム</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav mr-auto">
                    <li><a href="Dashboad.php">ダッシュボード</a></li>
                    <li><a href="#">売上登録</a></li>
                    <li class="active"><a href="#">売上検索<span class="sr-only">(current)</span></a></li>
                    <li><a href="#">アカウント登録</a></li>
                    <li><a href="#">アカウント検索</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">ログアウト</a></li>
                </ul>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->

    </nav>
    <div class="container-fruid">
        <form class="form-horizontal" action="./app/sale_edit.php" method="POST">
            <p>
                <h1>売上詳細編集確認</h1>
                <br>
            </p>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">販売日<span class="label label-default">必須</span></label>
                <div class="col-sm-4">
                    <input readonly type="text" class="form-control" name="date" value="<?php echo $_POST["date"]; ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">担当<span class="label label-default">必須</span></label>
                <div class="col-sm-5">
                    <select readonly class="form-control">
                        <option><?php echo $_POST["charge"]; ?></option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">商品カテゴリ<span class="label label-default">必須</span></label>
                <div class="col-sm-5">
                    <select readonly class="form-control">
                        <option><?php echo $_POST["category"]; ?></option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">商品名<span class="label label-default">必須</span></label>
                <div class="col-sm-5 col-offset-sm-2">
                    <input readonly type="text" class="form-control" name="name" value="<?php echo $_POST["name"]; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">単価<span class="label label-default">必須</span></label>
                <div class="col-sm-4 col-offset-sm-6">
                    <input readonly type="text" class="form-control" name="price" value="<?php echo $_POST["price"]; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">個数<span class="label label-default">必須</span></label>
                <div class="col-sm-4 col-offset-sm-6">
                    <input readonly type="text" class="form-control" name="count" value="<?php echo $_POST["count"]; ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">備考</label>
                <div class="col-sm-4 col-offset-sm-6">
                    <textarea readonly class="form-control" rows="4" name="remarks"><?php echo $_POST["remarks"]; ?></textarea>
                </div>
            </div>

            <input type="hidden" name="id" value="<?php echo $_POST["id"]; ?>">
            <input type="hidden" name="charge" value="<?php echo $charge; ?>">
            <input type="hidden" name="category" value="<?php echo $category; ?>">


            <div class="form-group">
                <div class="col-sm-offset-4">
                    <button type="submit" class="btn btn-primary btn-lg">✔OK</button>
                    <button type="button" class="btn btn-default btn-lg">キャンセル</button>
                </div>
            </div>
        </form>

    </div>
    <!--container-fruid-->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js"></script>
</body>

</html>