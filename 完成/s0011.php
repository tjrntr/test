<?php

include("./app/category.php");
include("./app/charge.php");
include("./app/regisration.php");

$db_cate = new Category;
$cateList = $db_cate->select();

$db_charge = new Charge;
$chargeList = $db_charge->select();

$link = new Link;
$link->next();

foreach ($chargeList as $row) {
    if ($_POST["charge"] == $row["charge_id"]) {
        $charge = $row["charge_name"];
    }
}

foreach ($cateList as $row2) {
    if ($_POST["category"] == $row2["category_id"]) {
        $category = $row2["category_name"];
    }
}
// var_dump($_POST["charge"]);
// echo "<br>";
// var_dump($_POST["category"]);
// echo "<br>";
// var_dump($charge);
// echo "<br>";
// var_dump($category);

?>
<!doctype html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>売上登録確認</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">物品売上管理システム</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav mr-auto">
                    <li><a href="./Dashboad.php">ダッシュボード</a></li>
                    <li class="active"><a href="./s0010.php">売上登録<span class="sr-only">(current)</span></a></li>
                    <li><a href="./s0020.php">売上検索</a></li>
                    <li><a href="./s0030.php">アカウント登録</a></li>
                    <li><a href="./s0040.php">アカウント検索</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="logout.php">ログアウト</a></li>
                </ul>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->

    </nav>
    <div class="container">
        <form class="form-horizontal" action="./app/sale_regisration2.php" method="POST">
            <p>
                <h1>売上登録確認</h1>
                <br>
            </p>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">販売日</label>
                <div class="col-sm-4">
                    <input readonly type="text" name="date" class="form-control" value="<?php echo $_POST['date']; ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">担当</label>
                <div class="col-sm-5">
                    <select readonly class="form-control" name="charge">
                        <option readonly><?php echo $charge; ?></option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">商品カテゴリ</label>
                <div class="col-sm-5">
                    <select readonly class="form-control" name="category">
                        <option readonly><?php echo $category; ?></option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">商品名</label>
                <div class="col-sm-5 col-offset-sm-2">
                    <input readonly type="text" name="productName" class="form-control" value="<?php echo $_POST["productName"]; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">単価</label>
                <div class="col-sm-4 col-offset-sm-6">
                    <input readonly type="text" name="price" class="form-control" value="<?php echo $_POST["price"]; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">個数</label>
                <div class="col-sm-4 col-offset-sm-6">
                    <input readonly type="text" name="count" class="form-control" value="<?php echo $_POST["count"]; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">小計</label>
                <div class="col-sm-4 col-offset-sm-6">
                    <input readonly type="text" class="form-control" value="<?php echo $_POST["price"] * $_POST["count"] ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">備考</label>
                <div class="col-sm-4 col-offset-sm-6">
                    <textarea readonly class="form-control" name="remarks" rows="4" ><?php echo $_POST["remarks"] ?></textarea>
                </div>
            </div>



            <div class="form-group">
                <div class="col-sm-offset-4">
                    <button type="submit" name="regist" class="btn btn-primary btn-lg">✔OK</button>
                    <a href="./s0010.php"><button type="button" class="btn btn-default btn-lg">キャンセル</button></a>
                </div>
            </div>
        </form>

    </div>
    <!--container-fruid-->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js"></script>
</body>

</html>