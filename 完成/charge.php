<?php
class Charge{
    /**
     * 全選択
     */
    function select(){
        try {
            $dbh = new PDO('mysql:host=localhost;dbname=system;charset=utf8mb4', "root", "");
            $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $sql = 'select * from charge';
            $res = $dbh->query($sql);
            $r = array();
            while ($row = $res->fetch(PDO::FETCH_ASSOC)) {
                $r[] = $row;
            }
            return $r;
        } catch (PDOException $e) {
            print "エラー!: " . $e->getMessage() . "<br/>";
            die(); 
        }
    }

    /**
     * 検索用
     */

    function search($name,$mail,$auth){
        try {
            $dbh = new PDO('mysql:host=localhost;dbname=system;charset=utf8mb4', "root", "");
            $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $sql = 'select * from charge where charge_name like :name';
            if(!empty($mail)){
                $sql .= " and charge_id = :mail";
            }
            if(!empty($auth)){
                $sql .= " and charge_auth_level = :auth";
            }
            $stmt = $dbh->prepare($sql);

            if(!empty($mail)){
                $stmt->bindValue(":name",$name,PDO::PARAM_STR);
            }
            if(!empty($auth)){
                $stmt->bindValue(":auth",$auth,PDO::PARAM_INT);
            }
            
            $stmt->execute();
            $r = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $r[] = $row;
            }
            return $r;
        } catch (PDOException $e) {
            print "エラー!: " . $e->getMessage() . "<br/>";
            die(); 
        }
    }
}

    /**
     * メールアドレス存在チェック
     */
    function validateMail($mail){
        try {
            $dbh = new PDO('mysql:host=localhost;dbname=system;charset=utf8mb4', "root", "");
            $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $sql = 'select count(*) from charge where charge_mail = :mail';
            $stmt = $dbh->prepare($sql);
            $stmt -> bindValue(":mail",$mail,PDO::PARAM_STR);
            $res = $stmt->execute();
            
            if($res > 0){
                return true;
            }else{
                return false;
            }
        } catch (PDOException $e) {
            print "エラー!: " . $e->getMessage() . "<br/>";
            die(); 
        }
    }

    /**
     * パスワードチェック
     */
    function validate($mail,$password){
        try {
            $dbh = new PDO('mysql:host=localhost;dbname=system;charset=utf8mb4', "root", "");
            $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $sql = 'select * from charge where charge_mail = :mail';
            $stmt = $dbh->prepare($sql);
            $stmt -> bindValue(":mail",$mail,PDO::PARAM_STR);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_OBJ);
            if($res){
                return $res->charge_password == $password;
            }else{
                return false;
            }            
        } catch (PDOException $e) {
            print "エラー!: " . $e->getMessage() . "<br/>";
            die(); 
        }
    }
}








?>
