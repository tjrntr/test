<?php
session_start();
// include("./app/regisration.php");

// $link = new Link2;
// $link->next2();
// ?>

<!doctype html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>アカウント登録確認</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">
</head>

<body>
        <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">物品売上管理システム</a>
                    </div>
        
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav mr-auto">
                                <li><a href="./Dashboad.html">ダッシュボード</a></li>
                                <li><a href="./s0010.php">売上登録</a></li>
                                <li><a href="./s0020.php">売上検索</a></li>
                                <li class="active"><a href="#">アカウント登録<span class="sr-only">(current)</span></a></li>
                                <li><a href="./s0040.php">アカウント検索</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="./logout.php">ログアウト</a></li> 
                        </ul>
                              
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
                
            </nav>
    <div class="container" >
        <form class="form-horizontal" action="./app/account_regisration.php" method="POST">
            <p> 
                <h1>アカウントを登録してもよろしいですか？</h1>
                <br>
            </p>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">氏名<span class="label label-default">必須</span></label>
                <div class="col-sm-4">
                    <input readonly type="text" name="name" class="form-control" id="inputEmail3" value="<?php echo $_POST["name"]; ?>">
                </div>
            </div>
             
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">メールアドレス<span class="label label-default">必須</span></label>
                <div class="col-sm-5 col-offset-sm-2">
                    <input readonly type="email" name="email" class="form-control" id="inputEmail3" value="<?php echo $_POST["email"]; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">パスワード<span class="label label-default">必須</span></label>
                <div class="col-sm-4 col-offset-sm-6">
                    <input readonly type="password" name="pass" class="form-control" value="<?php echo $_POST["pass"]; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">パスワード確認<span class="label label-default">必須</span></label>
                <div class="col-sm-4 col-offset-sm-6">
                    <input readonly type="password" name="repass" class="form-control" value="<?php echo $_POST["repass"]; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">権限<span class="label label-default">必須</span></label>
                <div class="form-group">

                <label class="radio-inline">
                        　<input disabled type="radio"  <?php if($_POST["auth"] == 1){ echo "checked";} ?> > 権限なし
                    </label>
                    <label class="radio-inline">
                        <input disabled type="radio" <?php if($_POST["auth"] == 2){ echo "checked";} ?>> 売上登録
                    </label>
                    <label class="radio-inline">
                        <input disabled type="radio" <?php if($_POST["auth"] == 3){ echo "checked";} ?> > アカウント登録
                    </label>
            </div>

            <input type="hidden" name="auth" value="<?php echo $_POST["auth"]; ?>">
           


    
    <div class="form-group">
        <div class="col-sm-offset-4">
        <button type="submit" class="btn btn-primary btn-lg">OK</button>
        <a href="./s0030.php"><button type="button" class="btn btn-default btn-lg">キャンセル</button></a>

        </div>
    </div>
    </form>

    </div>
    <!--container-fruid-->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ"
        crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js"></script>
</body>

</html>