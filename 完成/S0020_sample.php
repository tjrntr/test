<?php
include("./app/category.php");
include("./app/charge.php");

$db_cate = new Category;
$cateList = $db_cate->select();

$db_charge = new Charge;
$chargeList = $db_charge->select();

?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>テンプレート|物品売上管理システム</title>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/style.css">
</head>

<body>
    <!-- navbar -->
    <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">物品売上管理システム</a>
                </div>
    
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="C0020.php">ダッシュボード</a></li>
                        <li><a href="S0010.php">売上登録</a></li>
                        <li><a href="S0020.php">売上検索</a></li>
                        <li><a href="S0030.php">アカウント登録</a></li>
                        <li><a href="S0040.php">アカウント検索</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="C0010.php">ログアウト</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- navbar -->
    
        <div class="container">
			<div class="h1">売上検索条件入力</div>
			
			<form class="form-horizontal" action="./S0021.php" method="POST" >

				<div class="form-group">
					<label class="col-sm-3 control-label">販売日</label>
					
					<div class="col-sm-2">
						<input type="text" class="form-control" name="sales_day_from" placeholder="yyyy/mm/dd">
					</div>
					<div class="col-sm-1"><p class="form-control-static text-center">～</p></div>
					<div class="col-sm-2">
						<input type="text" class="form-control" name="sales_day_to" placeholder="yyyy/mm/dd">
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="charge">担当</label>
					<div class="col-sm-5">
						<select class="form-control" name="charge">
							<option value="">選択肢してください</option>
							<?php
							foreach($chargeList as $row){
								echo "<option value={$row['charge_id']}>";
								echo $row['charge_name'];
								echo "</option>";
							}
							?>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="product_category">商品カテゴリー</label>
					<div class="col-sm-5">
						<select class="form-control" name="product_category">
							<option value="">選択肢してください</option>
							<?php
							foreach($cateList as $row){
								echo "<option value={$row['category_id']}>";
								echo $row['category_name'];
								echo "</option>";
							}
							?>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="product_name">商品名 <span class="badge">部分一致</span></label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="product_name" placeholder="商品名">
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="remarks">備考 <span class="badge">部分一致</span></label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="remarks" placeholder="備考">
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-4 col-sm-8">
						<button type="submit" class="btn btn-primary" name="search">
							<span class="glyphicon glyphicon-search" aria-hidden="true"></span> 検　索
						</button>
						<button type="reset" class="btn btn-default">クリア</button>
					</div>
				</div>
			</form>
        </div>

    <script src="./js/jquery-1.12.4.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
</body>

</html>

