<?php
session_start();

    if (isset($_SESSION["accounts"])){
        header("location:./Dashboad.php");
    }

 if(isset($_GET["err"])){
     switch($_GET["err"]){
         case 1: $err = "メールアドレスを入力して下さい。";break;
         case 2: $err = "メールアドレスが長すぎます。"; break;
         case 3: $err = "メールアドレスを正しく入力して下さい。";break;
         case 4: $err = "パスワードを入力して下さい。"; break;
         case 5: $err = "パスワードが長すぎます。"; break;
         case 6: $err = "メールアドレスを正しく入力して下さい。"; break;
         case 7: $err = "メールアドレス、パスワードを正しく入力して下さい。"; break;
        
     }
 } 

?>
<!doctype html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ログイン｜物品売上管理システム</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">
</head>

<body class="bg-success">
    <form action = "./Dashboad.php" method="POST">
        <br>
        <br>
        <br>

        <div class="container-fluid">

            <div>
                <div class="text-center row">
                    <h3><b>物品売上管理システム</b></h3>
                </div>
                <div class="">
                    <form class="form-horizontal">
                        <div class="form-group row">
                            <div class="col-xs-4 col-xs-offset-4">
                                <input type="text" name="mail" class="form-control" placeholder="メールアドレス">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-4 col-xs-offset-4">
                                <input type="password" name="pass" class="form-control" placeholder="パスワード">
                            </div>

                        </div>
                </div>

                <div class="text-center">
                    <div class="col-xs-4 col-xs-offset-4">
                        
                        <button type="submit" name="btn" class="btn btn-primary btn-lg btn-block">ログイン</button> 
                
                    </div>
                </div>
                <?php
                if(isset($err)){
                echo '<center>
                    <h5><b>'.$err.'</b></h5>
                    </center>';
                }
                ?>
            </div>
    </form>





    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ"
        crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js"></script>
</body>

</html>