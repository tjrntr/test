<?php
session_start();

include("./app/category.php");
include("./app/charge.php");

$db_cate = new Category;
$cateList = $db_cate->select();

$db_charge = new Charge;
$chargeList = $db_charge->select();

// var_dump($_GET["err"]);


?>
<!doctype html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>売上登録</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">
</head>

<body>
        <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">物品売上管理システム</a>
                    </div>
        
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav mr-auto">
                            <li><a href="./Dashboad.php">ダッシュボード</a></li>
                            <li class="active"><a href="#">売上登録<span class="sr-only">(current)</span></a></li>
                            <li><a href="./s0020.php">売上検索</a></li>
                            <li><a href="./s0030.php">アカウント登録</a></li>
                            <li><a href="./s0040.php">アカウント検索</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="./logout.php">ログアウト</a></li> 
                        </ul>
                              
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
                
            </nav>
    <div class="container" >
        <form class="form-horizontal" action="./s0011.php" method="POST">
            <p> 
                <h1>売上登録</h1>
                <br>
            </p>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">販売日<span class="label label-default">必須</span></label>
                <div class="col-sm-4">
                    <input type="date" class="form-control" value="2019-01-01" name="date">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">担当<span class="label label-default">必須</span></label>
                <div class="col-sm-5">
                    <select class="form-control" name="charge">
                        <option>選択してください</option>
                        <?php
							foreach($chargeList as $row){
								echo "<option value={$row['charge_id']}>";
								echo $row['charge_name'];
								echo "</option>";
							}
							?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">商品カテゴリ<span class="label label-default">必須</span></label>
                <div class="col-sm-5">
                    <select class="form-control" name="category">
                        <option>選択してください</option>
                        <?php
							foreach($cateList as $row){
								echo "<option value={$row['category_id']}>";
								echo $row['category_name'];
								echo "</option>";
							}
							?>
                    </select>
                </div>
            </div>
             
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">商品名<span class="label label-default">必須</span></label>
                <div class="col-sm-5 col-offset-sm-2">
                    <input type="text" class="form-control" name="productName" placeholder="商品名">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">単価<span class="label label-default">必須</span></label>
                <div class="col-sm-4 col-offset-sm-6">
                    <input type="text" class="form-control" name="price" placeholder="単価">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">個数<span class="label label-default">必須</span></label>
                <div class="col-sm-4 col-offset-sm-6">
                    <input type="text" class="form-control" name="count" placeholder="個数">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">備考</label>
                <div class="col-sm-4 col-offset-sm-6">
                    <textarea class="form-control" rows="4" name="remarks" placeholder="備考"></textarea>
                </div>
            </div>
            
            <?php
                if(isset($_GET["err"])){
                foreach($_GET["err"] as $err){
                    echo "<h5>".$err."</h5>";
                }}
            ?>

    
    <div class="form-group">
        <div class="col-sm-offset-4">
            <button type="submit" class="btn btn-primary btn-lg">✔登録</button>

        </div>
    </div>
    </form>

    </div>
    <!--container-fruid-->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ"
        crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js"></script>
</body>

</html>