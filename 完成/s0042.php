<?php
session_start();
include("./app/charge.php");

$res = array();
if (isset($_GET["id"])) {
    $db = new Charge();
    $charge_id = $_GET["id"];
    $res = $db->id($charge_id);
}
// var_dump($res);
// echo "<br>";

foreach ($res as $row){

    $id=$row["charge_id"];
    $name = $row["charge_name"];
    $mail = $row["charge_mail"];
    $pass = $row["charge_password"];
    $auth = $row["charge_auth_level"];
}




?>

<!doctype html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>アカウント詳細編集</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">
</head>

<body>
        <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">物品売上管理システム</a>
                    </div>
        
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav mr-auto">
                                <li><a href="./Dashboad">ダッシュボード</a></li>
                                <li><a href="./s0010.php">売上登録</a></li>
                                <li><a href="./s0020.php">売上検索</a></li>
                                <li><a href="./s0030.php">アカウント登録</a></li>
                                <li class="active"><a href="#">アカウント検索<span class="sr-only">(current)</span></a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="./logout.php">ログアウト</a></li> 
                        </ul>
                              
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
                
            </nav>
    <div class="container" >
        <form class="form-horizontal" action="./s0043.php" method="POST">
            <p> 
                <h1>アカウント詳細編集</h1>
                <br>
            </p>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">氏名<span class="label label-default">必須</span></label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="name" value="<?php echo $name; ?>">
                </div>
            </div>
             
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">メールアドレス<span class="label label-default">必須</span></label>
                <div class="col-sm-5 col-offset-sm-2">
                    <input type="text" class="form-control" name="mail" value="<?php echo $mail; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">パスワード<span class="label label-default">必須</span></label>
                <div class="col-sm-4 col-offset-sm-6">
                    <input type="password" class="form-control" name="pass" value="<?php echo $pass; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">パスワード確認<span class="label label-default">必須</span></label>
                <div class="col-sm-4 col-offset-sm-6">
                    <input type="password" class="form-control" name="repass" value="<?php echo $pass; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">権限<span class="label label-default">必須</span></label>
                <div class="form-group">

                    <div class="radio-inline">
                      <input type="radio" value="1" name="auth" <?php if($auth == 1){echo "checked";} ?>>
                        <label for="man">権限なし</label>
                    </div>
                    <div class="radio-inline">
                      <input type="radio" value="2" name="auth" <?php if($auth == 2){echo "checked";} ?>>
                      <label for="woman">売上登録</label>
                    </div>
                    <div class="radio-inline">
                        <input type="radio" value="3" name="auth" <?php if($auth == 3){echo "checked";} ?>>
                        <label for="woman">販売登録</label>
                      </div>
                  </div>
            </div>

            <input type="hidden" name="id" value="<?php echo $id; ?>">
           


    
    <div class="form-group">
        <div class="col-sm-offset-4">
        <button type="submit" class="btn btn-primary btn-lg">✔更新</button>
        <button type="button" class="btn btn-default btn-lg">キャンセル</button>

        </div>
    </div>
    </form>

    </div>
    <!--container-fruid-->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ"
        crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js"></script>
</body>

</html>