<?php
session_start();
include("./app/sale2.php");

$res = array();
if (isset($_GET["id"])) {
    $db = new Sale2();
    $sale_id = $_GET["id"];
    $res = $db->select($sale_id);
}
foreach($res as $row){
    $id = $row["sale_id"];
}
// var_dump($id)
// var_dump($res);

?>

<!doctype html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>売上詳細表示</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">物品売上管理システム</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav mr-auto">
                    <li><a href="./Dashboad.php">ダッシュボード</a></li>
                    <li><a href="./s0010.php">売上登録</a></li>
                    <li class="active"><a href="#">売上検索<span class="sr-only">(current)</span></a></li>
                    <li><a href="./アカウント登録.html">アカウント登録</a></li>
                    <li><a href="./アカウント検索条件入力.html">アカウント検索</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">ログアウト</a></li>
                </ul>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->

    </nav>

    <div class="container">
        <form class="form-horizontal" method="POST" action="./s0025.php?id=<?php echo $id; ?>">
            <p>
                <h1>売上詳細表示</h1>
                <br>
            </p>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">販売日<span class="label label-default">必須</span></label>
                <div class="col-sm-4">
                    <?php
                    foreach ($res as $row) {
                        echo $row["sale_date"];
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">担当<span class="label label-default">必須</span></label>
                <div class="col-sm-5">
                    <?php
                    foreach ($res as $row) {
                        echo $row["charge_name"];
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">商品カテゴリ<span class="label label-default">必須</span></label>
                <div class="col-sm-5">
                    <?php
                    foreach ($res as $row) {
                        echo $row["category_name"];
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">商品名<span class="label label-default">必須</span></label>
                <div class="col-sm-5 col-offset-sm-2">
                    <?php
                    foreach ($res as $row) {
                        echo $row["product_name"];
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">単価<span class="label label-default">必須</span></label>
                <div class="col-sm-4 col-offset-sm-6">
                    <?php
                    foreach ($res as $row) {
                        echo $row["product_price"];
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">個数<span class="label label-default">必須</span></label>
                <div class="col-sm-4 col-offset-sm-6">
                    <?php
                    foreach ($res as $row) {
                        echo $row["product_count"];
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">備考</label>
                <div class="col-sm-4 col-offset-sm-6">
                    <?php
                    foreach ($res as $row) {
                        echo $row["sale_remarks"];
                    }
                    ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-4">

                    <a href="./s0023.php?id=<?php echo $id ?>"><button type="button" class="btn btn-primary btn-md">✔編集</button></a>
                    <button type="submit" class="btn btn-danger btn-md">✖削除</button>
                    <button type="button" class="btn btn-default btn-md">キャンセル</button>
                </div>
            </div>
        </form>

    </div>
    <!--container-fruid-->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js"></script>
</body>

</html>