<?php
include("./session.php");
$login = new Login;
$login->login();
?>

<!doctype html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboad</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">物品売上管理システム</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav mr-auto">
                    <li class="active"><a href="">ダッシュボード<span class="sr-only">(current)</span></a></li>
                    <li><a href="./s0010.php">売上登録</a></li>
                    <li><a href="./s0020.php">売上検索</a></li>
                    <li><a href="./s0030.php">アカウント登録</a></li>
                    <li><a href="./s0040.php">アカウント検索</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="./logout.php">ログアウト</a></li> 
                </ul>
                      
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
        
    </nav>

    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ"
        crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js"></script>
</body>

</html>