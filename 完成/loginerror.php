<?php

try {
    $dbh = new PDO('mysql:host=localhost;dbname=system;charset=utf8mb4', "root", "");
    $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = 'select* from charge';
    $res = $dbh->query($sql);
    $r = array();
    while ($row = $res->fetch(PDO::FETCH_ASSOC)) {
        $r[] = $row;
    }

    if (isset($_POST["btn"])) {
        foreach ($r as $e) {
                if(empty($_POST["mail"])){
                    $err = "メールアドレスを入力して下さい。";
                }elseif(strlen($_POST["mail"]) >= 101){
                    $err = "メールアドレスが長すぎます。";
                }elseif(!filter_var($_POST["mail"], FILTER_VALIDATE_EMAIL)){
                    $err = "メールアドレスを正しく入力して下さい。";
                }elseif(empty($_POST["password"])){
                    $err = "パスワードを入力して下さい。";
                }elseif(strlen($_POST["password"]) >= 31){
                    $err = "パスワードが長すぎます。";
                }elseif(!$e["charge_mail"]==$_POST["mail"]){
                    $err = "メールアドレスを正しく入力して下さい。";
                }elseif(!$e["charge_mail"] == $_POST["mail"] && !$e["charge_password"] == $_POST["pass"]){
                    $err = "メールアドレス、パスワードを正しく入力して下さい。";
                }
            }
        }
    
}catch(PDOException $e){
    echo "ERROR";
}





?>