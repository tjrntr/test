<?php
session_start();
?>
<!doctype html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>アカウント登録</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">物品売上管理システム</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav mr-auto">
                    <li><a href="./Dashboad.php">ダッシュボード</a></li>
                    <li><a href="./s0010.php">売上登録</a></li>
                    <li><a href="./s0020.php">売上検索</a></li>
                    <li class="active"><a href="#">アカウント登録<span class="sr-only">(current)</span></a></li>
                    <li><a href="./s0040.php">アカウント検索</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="./logout.php">ログアウト</a></li>
                </ul>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->

    </nav>
    <div class="container">
        <form class="form-horizontal" action="./s0031.php" method="POST">
            <p>
                <h1>アカウント登録</h1>
                <br>
            </p>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">氏名<span class="label label-default">必須</span></label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="name" placeholder="氏名">
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">メールアドレス<span class="label label-default">必須</span></label>
                <div class="col-sm-5 col-offset-sm-2">
                    <input type="email" class="form-control" id="inputEmail3" name="email" placeholder="メールアドレス">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">パスワード<span class="label label-default">必須</span></label>
                <div class="col-sm-4 col-offset-sm-6">
                    <input type="password" class="form-control" name="pass" placeholder="パスワード">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">パスワード確認<span class="label label-default">必須</span></label>
                <div class="col-sm-4 col-offset-sm-6">
                    <input type="password" class="form-control" name="repass" placeholder="パスワード確認">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">権限<span class="label label-default">必須</span></label>
                <div class="form-group">
                    
            
                 　   <label class="radio-inline">
                        <input type="radio" name="auth" id="inlineRadio1" value="1"> 権限なし
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="auth" id="inlineRadio2" value="2"> 売上登録
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="auth" id="inlineRadio3" value="3"> アカウント登録
                    </label>
                </div>



                <div class="form-group">
                    <div class="col-sm-offset-4">
                        <button type="submit" class="btn btn-primary btn-lg">✔登録</button>
                    </div>
                </div>
        </form>

    </div>
    <!--container-fruid-->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js"></script>
</body>

</html>