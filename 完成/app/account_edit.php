<?php
// echo $_POST["id"];
// echo $_POST["name"];

try {
    $dbh = new PDO('mysql:host=localhost;dbname=system;charset=utf8mb4', "root", "");
    $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = 'update charge set charge_name=:charge_name, charge_mail=:charge_mail, charge_password=:charge_password,
    charge_auth_level=:charge_auth_level where charge_id=:charge_id';

    $id = $_POST["id"];
    $name = $_POST["name"];
    $mail = $_POST["mail"];
    $pass = $_POST["pass"];
    $auth =$_POST["auth"];

    $stmt = $dbh->prepare($sql);
    $stmt->bindValue(':charge_id', $id, PDO::PARAM_INT);
    $stmt->bindValue(':charge_name', $name, PDO::PARAM_STR);
    $stmt->bindValue(':charge_mail', $mail, PDO::PARAM_INT);
    $stmt->bindValue(':charge_password', $pass, PDO::PARAM_INT);
    $stmt->bindValue(':charge_auth_level', $auth, PDO::PARAM_STR);
    

    $result = $stmt->execute();

    header ("location: ../s0040.php");
} catch (PDOException $e) {
    print "エラー!: " . $e->getMessage() . "<br/>";
    die();
}
?>
