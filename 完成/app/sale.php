<?php
include("./app/function.php");

class Sale{
    /**
     * 一覧表示（検索）
     */
    function select($sale_date,$sale_date2,$charge_id,$category_id,$product_name,$sale_remarks){
        try {
            $dbh = new PDO('mysql:host=localhost;dbname=system;charset=utf8mb4', "root", "");
            $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $sql = 'select * from sale,category,charge where sale.category_id=category.category_id and sale.charge_id = charge.charge_id';
            if(!empty($sale_date)){    
                $sale_date = dateFormat1($sale_date);
                $sql .= " and sale_date >= :sale_date";
            }
            if(!empty($sale_date2)){
                $sale_date2 = dateFormat1($sale_date2);
                $sql .= " and sale_date <= :sale_date2";
            }
            if(!empty($charge_id)){
                $sql .= " and sale.charge_id = :charge_id";
            }
            if(!empty($category_id)){
                $sql .= " and sale.category_id = :category_id";
            }
            if(!empty($product_name)){
                $sql .= " and product_name like :product_name";
            }
            if(!empty($sale_remarks)){
                $sql .= " and sale_remarks like :sale_remarks";

            }

            $stmt = $dbh->prepare($sql);
            
            if(!empty($sale_date)){
                $stmt->bindValue(":sale_date",$sale_date,PDO::PARAM_STR);
            }
            if(!empty($sale_date2)){
                $stmt->bindValue(":sale_date2",$sale_date2,PDO::PARAM_STR);
            }
            if(!empty($charge_id)){
                $stmt->bindValue(":charge_id",$charge_id,PDO::PARAM_INT);
            }
            if(!empty($category_id)){
                $stmt->bindValue(":category_id",$category_id,PDO::PARAM_INT);
            }
            if(!empty($product_name)){
                $stmt->bindValue(":product_name","%".$product_name."%",PDO::PARAM_STR);
            }
            if(!empty($sale_remarks)){
                $stmt->bindValue(":sale_remarks","%".$sale_remarks."%",PDO::PARAM_STR);
            }
            
            $stmt->execute();
            $r = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $r[] = $row;
            }
            return $r;
        } catch (PDOException $e) {
            print "エラー!: " . $e->getMessage() . "<br/>";
            die(); 
        }
    }
}

?>