<?php
class Sale2{
    /**
     * ID検索用
     */
    function select($sale_id){
        try {
            $dbh = new PDO('mysql:host=localhost;dbname=system;charset=utf8mb4', "root", "");
            $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $sql = 'select * from sale,category,charge where sale.category_id=category.category_id and sale.charge_id = charge.charge_id';

            if(!empty($sale_id)){
                $sql .= " and sale.sale_id = :sale_id";
            }

            $stmt = $dbh->prepare($sql);
            
            if(!empty($sale_id)){
                $stmt->bindValue(":sale_id",$sale_id,PDO::PARAM_INT);
            }
            
            $stmt->execute();
            $r = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $r[] = $row;
            }
            return $r;
        } catch (PDOException $e) {
            print "エラー!: " . $e->getMessage() . "<br/>";
            die(); 
        }
    }
}

?>