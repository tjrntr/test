<?php
session_start();
include("./charge.php");

$db_charge = new Charge;
$chargeList = $db_charge->select();


//アカウント登録
        
        try {
            $dbh = new PDO('mysql:host=localhost;dbname=system;charset=utf8mb4', "root", "");
            $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $sql = 'insert into charge(charge_name,charge_mail,charge_password,charge_auth_level)
                    values(:charge_name,:charge_mail,:charge_password,:charge_auth_level)';
                    
                    $charge_name = $_POST["name"];
                    $charge_mail = $_POST["email"];
                    $charge_password = $_POST["pass"];
                    $charge_auth_level = $_POST["auth"];


            $stmt = $dbh->prepare($sql);
            $stmt->bindValue(':charge_name',$charge_name,PDO::PARAM_STR);
            $stmt->bindValue(':charge_mail',$charge_mail,PDO::PARAM_STR);
            $stmt->bindValue(':charge_password',$charge_password,PDO::PARAM_STR);
            $stmt->bindValue(':charge_auth_level',$charge_auth_level,PDO::PARAM_INT);
            

            $result = $stmt->execute();
            
            header("location: ../s0030.php");

        } catch (PDOException $e) {
            print "エラー!: " . $e->getMessage() . "<br/>";
            die(); 
        }  
    
?>