<?php
// echo $_POST["charge"];
// echo $_POST["category"];

try {
    $dbh = new PDO('mysql:host=localhost;dbname=system;charset=utf8mb4', "root", "");
    $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = 'update sale set sale_date=:sale_date, charge_id=:charge_id, category_id=:category_id,
    product_name=:product_name, product_price=:product_price, product_count=:product_count, sale_remarks=:sale_remarks
    where sale_id=:sale_id';

    $sale_id = $_POST["id"];
    $sale_date = $_POST["date"];
    $charge_id = $_POST["charge"];
    $category_id =$_POST["category"];
    $product_name =$_POST["name"];
    $product_price =$_POST["price"];
    $product_count =$_POST["count"];
    $sale_remarks =$_POST["remarks"];

    $stmt = $dbh->prepare($sql);
    $stmt->bindValue(':sale_id', $sale_id, PDO::PARAM_INT);
    $stmt->bindValue(':sale_date', $sale_date, PDO::PARAM_STR);
    $stmt->bindValue(':charge_id', $charge_id, PDO::PARAM_INT);
    $stmt->bindValue(':category_id', $category_id, PDO::PARAM_INT);
    $stmt->bindValue(':product_name', $product_name, PDO::PARAM_STR);
    $stmt->bindValue(':product_price', $product_price, PDO::PARAM_INT);
    $stmt->bindValue(':product_count', $product_count, PDO::PARAM_INT);
    $stmt->bindValue(':sale_remarks', $sale_remarks, PDO::PARAM_STR);

    $result = $stmt->execute();

    header ("location: ../s0020.php");
} catch (PDOException $e) {
    print "エラー!: " . $e->getMessage() . "<br/>";
    die();
}
?>
