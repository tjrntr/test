<?php
include("./sale2.php");

$res = array();
if (isset($_GET["id"])) {
    $db = new Sale2();
    $sale_id = $_GET["id"];
    $res = $db->select($sale_id);
}
// var_dump($res);

foreach ($res as $row) {
    $delete_id = $row["sale_id"];
}

try {
    $dbh = new PDO('mysql:host=localhost;dbname=system;charset=utf8mb4', "root", "");
    $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = 'delete from sale where sale_id = :sale_id';

    $sale_id = $delete_id;

    $stmt = $dbh->prepare($sql);
    $stmt->bindValue(':sale_id', $sale_id, PDO::PARAM_STR);
    
    $result = $stmt->execute();
    
    header("location: ../s0020.php");
} catch (PDOException $e) {
    print "エラー!: " . $e->getMessage() . "<br/>";
    die();
}


?>
