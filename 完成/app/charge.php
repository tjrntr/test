<?php
class Charge{
    function select(){
        try {
            $dbh = new PDO('mysql:host=localhost;dbname=system;charset=utf8mb4', "root", "");
            $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $sql = 'select * from charge';
            $res = $dbh->query($sql);
            $r = array();
            while ($row = $res->fetch(PDO::FETCH_ASSOC)) {
                $r[] = $row;
            }
            return $r;
        } catch (PDOException $e) {
            print "エラー!: " . $e->getMessage() . "<br/>";
            die(); 
        }
    }
    /**
     * 検索用
     */

    function search($name,$mail,$auth){
        try {
            $dbh = new PDO('mysql:host=localhost;dbname=system;charset=utf8mb4', "root", "");
            $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $sql = 'select * from charge where 1=1';
            if(!empty($name)){
                $sql .= " and charge_name like :charge_name";
            }
            if(!empty($mail)){
                $sql .= " and charge_mail = :charge_mail";
            }
            if(!empty($auth)){
                $sql .= " and charge_auth_level = :charge_auth_level";
            }
            $stmt = $dbh->prepare($sql);
            
            if(!empty($name)){
                $stmt->bindValue(":charge_name",$name,PDO::PARAM_STR);
            }
            if(!empty($mail)){
                $stmt->bindValue(":charge_mail",$mail,PDO::PARAM_STR);
            }
            if(!empty($auth)){
                $stmt->bindValue(":charge_auth_level",$auth,PDO::PARAM_INT);
            }
            
            $stmt->execute();
            $r = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $r[] = $row;
            }
            return $r;
        } catch (PDOException $e) {
            print "エラー!: " . $e->getMessage() . "<br/>";
            die(); 
        }
    }

    
/**
     * ID検索用
     */

    function id($charge_id){
        try {
            $dbh = new PDO('mysql:host=localhost;dbname=system;charset=utf8mb4', "root", "");
            $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $sql = 'select * from charge where 1=1';
            
            if(!empty($charge_id)){
                $sql .= " and charge_id = :charge_id";
            }

            $stmt = $dbh->prepare($sql);

            if(!empty($charge_id)){
            $stmt->bindValue(":charge_id",$charge_id,PDO::PARAM_INT);
            }
            
            
            
            $stmt->execute();
            $r = array();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $r[] = $row;
            }
            return $r;
        } catch (PDOException $e) {
            print "エラー!: " . $e->getMessage() . "<br/>";
            die(); 
        }
    }
}
?>