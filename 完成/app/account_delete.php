<?php

include("./charge.php");

$res = array();
if (isset($_GET["id"])) {
    $db = new Charge();
    $charge_id = $_GET["id"];
    $res = $db->id($charge_id);
}
// var_dump($res);


foreach ($res as $row) {
    $delete_id = $row["charge_id"];
}


try {
    $dbh = new PDO('mysql:host=localhost;dbname=system;charset=utf8mb4', "root", "");
    $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = 'delete from charge where charge_id = :charge_id';

    $charge_id = $delete_id;

    $stmt = $dbh->prepare($sql);
    $stmt->bindValue(':charge_id', $charge_id, PDO::PARAM_INT);
    
    $result = $stmt->execute();
    
    header("location: ../s0041.php");
} catch (PDOException $e) {
    print "エラー!: " . $e->getMessage() . "<br/>";
    die();
}

?>