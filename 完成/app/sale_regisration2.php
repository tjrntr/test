<?php
include("./category.php");
include("./charge.php");

$db_cate = new Category;
$cateList = $db_cate->select();

$db_charge = new Charge;
$chargeList = $db_charge->select();

foreach ($chargeList as $row) {
    if ($_POST["charge"] == $row["charge_name"]) {
        $charge = $row["charge_id"];
    }
}

foreach ($cateList as $row2) {
    if ($_POST["category"] == $row2["category_name"]) {
        $category = $row2["category_id"];
    }
}

//売上登録
        
        try {
            $dbh = new PDO('mysql:host=localhost;dbname=system;charset=utf8mb4', "root", "");
            $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $sql = 'insert into sale(sale_date,charge_id,category_id,product_name,product_price,product_count,sale_remarks)
                    values(:sale_date,:charge_id,:category_id,:product_name,:product_price,:product_count,:sale_remarks)';
                    
                    $sale_date = $_POST["date"];
                    $charge_id = $charge;
                    $category_id = $category;
                    $product_name = $_POST["productName"];
                    $product_price = $_POST["price"];
                    $product_count = $_POST["count"];
                    $sale_remarks = $_POST["remarks"];

            $stmt = $dbh->prepare($sql);
            $stmt->bindValue(':sale_date',$sale_date,PDO::PARAM_STR);
            $stmt->bindValue(':charge_id',$charge_id,PDO::PARAM_INT);
            $stmt->bindValue(':category_id',$category_id,PDO::PARAM_INT);
            $stmt->bindValue(':product_name',$product_name,PDO::PARAM_STR);
            $stmt->bindValue(':product_price',$product_price,PDO::PARAM_STR);
            $stmt->bindValue(':product_count',$product_count,PDO::PARAM_STR);
            $stmt->bindValue(':sale_remarks',$sale_remarks,PDO::PARAM_STR);

            $result = $stmt->execute();
            header("location: ../s0010.php");

        } catch (PDOException $e) {
            print "エラー!: " . $e->getMessage() . "<br/>";
            die(); 
        }  
    

?>